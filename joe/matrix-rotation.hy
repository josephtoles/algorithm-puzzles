#!/home/joseph/code/algorithm-puzzles/joe/env/bin/hy

;; Important notice
;; As a convention, the first index in everything indicates row, the second indicates column
;; This is in reverse to traditional x-y coordinates.

;; test data

(setv test-matrix
      [[ 1  2  3  4]
       [ 5  6  7  8]
       [ 9 10 11 12]
       [13 14 15 16]
       [17 18 19 20]])

;; get matrix vectors and dimensions

(defn num-rows [matrix]
  "counts how many rows are in a matrix"
  (len matrix))

(defn num-cols [matrix]
  "counts how many columns are in a matrix"
  (len (. matrix [0])))

(defn get-row [row-index matrix]
  "returns a row of a matrix as a list"
  (. matrix [row-index]))

(defn get-col [col-index matrix]
  "returns a column of a matrix as a list"
  (map (fn [row-index]
         (. matrix [row-index] [col-index]))
       (range (num-rows matrix))))

;; matrix transformations

(defn make-matrix [get-element num-rows num-cols]
  "Creates a matrix from function get-element which takes two parameters x and y"
  (defn make-row [row-index]
    (list (map (fn [col-index] (get-element row-index col-index)) (range num-cols))))
  (list (map make-row (range num-rows))))

(defn transpose [matrix]
  (make-matrix
    (fn [r c]
      (. matrix [c] [r]))
    (num-cols matrix)
    (num-rows matrix)))

(defn reflect-horizontal [matrix]
  (make-matrix
    (fn [x y]
      (. matrix [x] [(- (num-cols matrix) y 1)]))
    (num-rows matrix)
    (num-cols matrix)))

(defn rotate-clockwise [matrix]
  (reflect-horizontal (transpose matrix)))

(defn rotate-counterclockwise [matrix]
  (transpose (reflect-horizontal matrix)))

;; print data

(defn print-matrix [matrix]
  "prints a matrix prettily"
  (for [row matrix]
    (print (list (map str row)))))

;; test code

(print "test matrix")
(print-matrix test-matrix)

(print "generated matrix")
(print-matrix (make-matrix (fn [x y] (+ x y)) 3 3))

(print "original matrix, transposed")
(print-matrix (transpose test-matrix))

(print "original matrix, reflected horizontally")
(print-matrix (reflect-horizontal test-matrix))

(print "rotated 90 degrees clockwise")
(print-matrix (rotate-clockwise test-matrix))

(print "rotated 90 degrees counterclockwise")
(print-matrix (rotate-counterclockwise test-matrix))
