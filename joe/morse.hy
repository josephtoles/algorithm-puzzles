#!/home/joseph/code/algorithm-puzzles/joe/env/bin/hy

(defclass TreeNode [object]
  [val 0
   left None
   right None]
  (defn --init-- [self x &optional [left None] [right None]]
    (setv self.val x)
    (setv self.left left)
    (setv self.right right)))

(defn print-tree [tree]
  (if tree
      (do
        (print tree.val)
        (print-tree tree.left)
        (print-tree tree.right))))

(setv tree-1 (TreeNode 1
                       :left (TreeNode 3
                                       :left (TreeNode 5))
                       :right (TreeNode 2)))

(setv tree-2 (TreeNode 2
                       :left (TreeNode 1
                                       :right (TreeNode 4))
                       :right (TreeNode 3
                                        :right (TreeNode 7))))

(print "Tree 1")
(print-tree tree-1)
(print "Tree 2")
(print-tree tree-2)

(defn merge-trees [t1 t2]
  (or (and t1 t2 (TreeNode (+ t1.val t2.val)
                           :left (merge-trees t1.left t2.left)
                           :right (merge-trees t1.right t2.right)))
      t1 t2))

(print "merged")
(print-tree (merge-trees tree-1 tree-2))

